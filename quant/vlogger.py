import logging


class VLogger:
    logger = logging.getLogger()
    v = 0

    @staticmethod
    def vlog(v: int, msg: str):
        if v <= VLogger.v:
            VLogger.logger.info(msg)


    @staticmethod
    def error(msg: str):
        VLogger.logger.error(msg)

    
    @staticmethod
    def warning(msg: str):
        VLogger.logger.warning(msg)

    @staticmethod
    def info(msg: str):
        VLogger.logger.info(msg)