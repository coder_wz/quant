# quant

#### 介绍

一些个人自用的自动交易代码以及策略回测代码。

策略回测主要是针对网格交易和可转债。回测框架基于rqlapha。

![可转债策略回测](quant/backtest/conbond.png)
![网格交易信号](quant/testdata/enhanced.png)

#### 安装教程

1. pip install -e .

#### 使用说明

1. 网格交易，可以参考 examples/grid-futu.py。用于在富途上对HK.00700进行网格交易。Futu的setup可以参考[这里](https://t.zsxq.com/QfyJ2NV)。Example usage:
   ./examples/grid-futy.py --code='HK.00700' --strategy='test'
2. 可转债的回测。Example usage: ./backtest/rqalpha/backtest.py --name=conbond

#### 目录说明

* backtest - 回测相关的代码。juejin目录下的代码使用的是掘金量化。rqalpha内的是基于rqalpha已经一个扩展的模块
* data - 数据收集的代码，主要是可转债。数据来源：集思录，聚宽，米筐
* examples - 交易的例子，掘金和futu的。
* rqalpha - 从本地读取行情数据的扩展模块，主要是为了支持可转债的回测（默认不支持）
* testdata - 单元测试的数据
* 其他 - 策略文件以及画图

#### TODO

* (DISCARD) Rqalpha event for parallel runs to show progress using tqdm
* (DISCARD, 就用最后一笔价格作为基准) Last deal info is not correct when they are filled separately (market order)
* (DONE) Add benchmark for hk grid
* (DONE) Add filtering condition to avoid selecting conbonds that are close to maturity
* (DONE) Adjust grid state based on order (since it is market order)
* (DONE) Continue with change of hands_per_grid
* (DONE) Fix annotation format in plots
* (DONE) Fix dates in the trade signal plot for 1m frequency
* (DONE) Handle grid breakage
* (DONE) Handle grid_id issue (when it bounces back from lower grid and buy, similarly for the sell case)
* (DONE) Merge rqalpha backtest code
* (DONE) Refresh 1m level data
* (DONE) Support 1m backtest for all stocks
* (DONE) Update call sites of read_csv
* (DONE) Use conditional order / turning point grid for HK.00700
* (DONE) Use local source mode for unit test
* (DONE) Use rt data for futu grid
* (DONE) Figure out why rqalpha pass in 15:30:00 in get_bar
* (DONE) Use yaml instead of json
* (DONE) 处理网格破网的情况
* (DONE) 添加转债等权指数
* (DONE) 给与溢价率和价格不同权重
* (DONE) Move some logic from data source to eval in strategy
* (DONE) Run backtest in parallel
* (DONE) Add trader class with interface
* (DONE) Convert order_book_id for cron
* (DONE) Plot 最大回撤时间段
* (DONE) See why GridTest fails, if run together: because of 'nonlocal data_proxy'
* (DONE) Use DataFrame.rank()
* (DONE) Use all_instruments + bar_dict to replace bars and instruments in strategy (slow though)
* (DONE) Use jisilu.py for CB.IDX
* (DONE) Use richquent.py as jisilu.py
* (DONE) Use vectorized eval: Idea: foo: '{bar} + 100 * {x}' -> df['foo'] = '{bar} + 100 * {x}'.format(bar='df.bar', x='df.x')
* (DONE) Use vectorized for other as well
* (DONE) declare callable types
* (DONE) 低溢价策略收益率变化：是否过滤已公告强赎对低溢价率策略收益率有极大影响，应该过滤即将强赎或者到期
* (DONE) 移除按月轮动，可能无法过滤强赎(强赎公告到停止交易可能不足一个月)
* (DONE) Incremental update of ricequant data (after license expire)
* Calculate grid based on MA / ATR etc.
* Explore the rqalpha py libs from pypi
* Fix ploting large number of results
* For 1m data, should remove indicators, suspended etc., not reasonable to have that
* Interactive plot, better visualization of trade signals (plotly, tradingview, mpl3d, pyecharts etc.)
* Make sure no error in backtest
* Plot signals from different strategies into one graph
* 低溢价:如果为负值，是否因为正股大跌。
* 转债策略：增加活跃度因子，考虑正股最近表现
* (DONE) 剩余规模因子
* (DONE) Explore juejin multi accounts (not supported in backtest), cache day data, cache signals
* (DONE) premium_rt of jisilu changed
* (DONE) need to update from redeem for delisted
* (DONE) Wait for close to finish, before open position
* (DONE) 获取tick数据（比如买三，卖三），可转债不支持市价委托，尽快成交
* order_target有bug，取消数量小于10的order