import pathlib

from pip._internal.network.session import PipSession
from pip._internal.req import parse_requirements
from setuptools import (
    find_packages,
    setup,
)

with open(
        pathlib.Path(__file__).parent.joinpath('quant', 'VERSION.txt'),
        'rb') as f:
    version = f.read().decode('ascii').strip()

setup(
    name='quant',  # mod名
    version=version,
    description='My quant library',
    packages=find_packages(exclude=[]),
    author='paulhybryant',
    author_email='paulhybryant@gmail.com',
    license='Apache License v2',
    package_data={'quant': ['py.typed']},
    url='https://gitee.com/paulhybryant/quant',
    install_requires=[
        str(ir.requirement)
        for ir in parse_requirements("requirements.txt", session=PipSession())
    ],
    zip_safe=False,
    classifiers=[
        'Programming Language :: Python',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: Unix',
    ],
)
